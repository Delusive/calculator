package com.epam.unit_tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

public class OperationsTest {

    @ParameterizedTest
    @DisplayName("Some expected values for ADDITION")
    @CsvSource(value = {"1:2:3", "0:0:0", "100:512:612", "-4:-3:-7"}, delimiter = ':')
    public void checkSomeExpectedValuesForAddition(double a, double b, double expectedResult) {
        assertEquals(expectedResult, Operations.addition(a, b));
    }

    @ParameterizedTest
    @DisplayName("Some expected values for SUBTRACTION")
    @CsvSource(value = {"94918:90000:4918", "0:100:-100", "-5:-10:5"}, delimiter = ':')
    public void checkSomeExpectedValuesForSubtraction(double a, double b, double expectedResult) {
        assertEquals(expectedResult, Operations.subtraction(a, b));
    }

    @ParameterizedTest
    @DisplayName("Some expected values for DIVISION")
    @CsvSource(value = {"10:5:2", "999:9:111", "90:1:90", "5:2.5:2", "0:993142:0"}, delimiter = ':')
    public void checkSomeExpectedValuesForDivision(double a, double b, double expectedResult) {
        assertEquals(expectedResult, Operations.division(a, b));
    }

    @Test
    @DisplayName("Should return infinity")
    public void shouldReturnInfinity() {
        assertTrue(Double.isInfinite(Operations.division(1, 0)));
    }

    @ParameterizedTest
    @DisplayName("Some expected values for MULTIPLICATION")
    @CsvSource(value = {"0:0:0", "0:39849:0", "1:8:8", "9:8:72", "100:200:20000"}, delimiter = ':')
    public void checkSomeExpectedValuesForMultiplication(double a, double b, double expectedResult) {
        assertEquals(expectedResult, Operations.multiplication(a, b));
    }

    @ParameterizedTest
    @DisplayName("Some expected result for SQUARE_ROOT")
    @CsvSource(value = {"1:1", "4:2", "100:10", "1024:32", "0:0"}, delimiter = ':')
    public void checkSomeExpectedValuesForSquareRoot(double a, double expectedResult) {
        assertEquals(expectedResult, Operations.squareRoot(a));
    }

    @Test
    @DisplayName("Should return NaN")
    public void shouldReturnNan() {
        assertTrue(Double.isNaN(Operations.squareRoot(-1)));
    }

}
