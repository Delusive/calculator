package com.epam.unit_tests;

import com.epam.unit_tests.exception.IllegalExpressionException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    @ParameterizedTest
    @DisplayName("Check for correct evaluation of expression only with ADDITION and SUBTRACTION")
    @CsvSource(value = {"1 + 2 - 3 + 4 - 5 + 10 - 7|2", "2.5 + 3.5 - 1.2|4.8"}, delimiter = '|')
    public void onlyAdditionAndSubtraction(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @DisplayName("Check for correct evaluation of expression only with DIVISION and MULTIPLICATION")
    @CsvSource(value = {"1 / 2 * 4 / 2|1", "2 * 4 / 8 * 100 / 10|10", "1 / 2 / 2 / 2 / 2 / 2|0.03125", "2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2|1024"}, delimiter = '|')
    public void onlyDivisionAndMultiplication(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @DisplayName("Check for correct evaluation of expression only with ADDITION, SUBTRACTION and one-digit SQUARE_ROOT")
    @CsvSource(value = {"1 + sqrt4 + sqrt9 - sqrt4|4", "0 - sqrt4 + sqrt4|0"}, delimiter = '|')
    public void onlyAdditionSubtractionAndOneDigitSquareRoot(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @DisplayName("Check for correct evaluation of expression only with multi-digit SQUARE_ROOT, ADDITION and SUBTRACTION")
    @CsvSource(value = {"1 + sqrt16 + sqrt1024 - sqrt729|10", "sqrt441 - sqrt400 + sqrt100|11"}, delimiter = '|')
    public void onlyAdditionSubtractionAndMultiDigitSquareRoot(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @DisplayName("High-priority operations are at the beginning of the expression")
    @CsvSource(value = {"2 * 2 / 2 * 4 * sqrt4 + 4 - 10 - 2|8", "4 * 4 * 1 * 2 / 32 + 1 - 0|2"}, delimiter = '|')
    public void highPriorityOperationsAreAtTheBeginningOfTheExpression(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @DisplayName("Should throw IllegalExpressionException")
    @NullSource
    @ValueSource(strings = {"", "1 +", "-", "1 + +", "-1 - 1 (2)"})
    public void shouldThrowIllegalExpressionException(String param) {
        assertThrows(IllegalExpressionException.class, () -> new Calculator(param));
    }

    @ParameterizedTest
    @DisplayName("Should evaluate any expression with allowed operators")
    @CsvSource(value = {"2 * 2 / 2 * 4 * sqrt4 + 4 - 10 - 2|8", "4 * 4 * 1 * 2 / 32 + 1 - 0|2", "4.5 / 4.5 + 1 - sqrt6.25 + 1|0.5"}, delimiter = '|')
    public void shouldEvaluateAnyAllowedExpressions(String expression, double expectedResult) throws IllegalExpressionException {
        Calculator calc = new Calculator(expression);
        Double result = calc.evaluate();
        assertEquals(expectedResult, result);
    }

}
