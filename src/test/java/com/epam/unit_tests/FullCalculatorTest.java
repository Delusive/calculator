package com.epam.unit_tests;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("Calculator test-suite")
@SelectPackages("com.epam.unit_tests")
public class FullCalculatorTest  {

}
