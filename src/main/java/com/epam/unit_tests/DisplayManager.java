package com.epam.unit_tests;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

/**
 * Отвечает за вывод информации в консоль.
 */
public class DisplayManager {
    private Logger logger;

    /**
     * Конструктор ¯\_(ツ)_/¯.
     *
     * @param name Название логгера.
     */
    public DisplayManager(String name) {
        logger = LogManager.getLogger(name);
        Configurator.setLevel(name, Level.INFO);
    }

    /**
     * Вывод информации на экран.
     *
     * @param msg Сообщение, которое необходимо вывести.
     */
    public void info(String msg) {
        logger.log(Level.INFO, msg);
    }

    /**
     * Вывод предупреждения в консоль.
     *
     * @param warn Текст предупреждения, который необходимо вывести.
     */
    public void warn(String warn) {
        logger.log(Level.WARN, warn);
    }

    /**
     * Вывод ошибки в консоль.
     *
     * @param err Текст ошибки, который необходимо вывести.
     */
    public void error(String err) {
        logger.log(Level.ERROR, err);
    }
}
