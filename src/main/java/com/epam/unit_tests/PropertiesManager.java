package com.epam.unit_tests;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Отвечает за работу с файлом конфигурации.
 */
public class PropertiesManager {
    private DisplayManager dm = new DisplayManager(getClass().getName());
    private Properties props = new Properties();
    private InputStream is;

    /**
     * Это тоже конструктор!!
     *
     * @param fileName Имя файла конфигурации.
     */
    public PropertiesManager(String fileName) {
        is = getClass().getResourceAsStream(fileName);
        if (is == null) {
            dm.error("Properties file not found!");
            return;
        }
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Получение значение параметра из файла конфигурации.
     *
     * @param prop Параметр, значение которого необходимо получить.
     * @return Значение указанного параметра.
     */
    public String getProperty(String prop) {
        if (!props.containsKey(prop)) {
            new DisplayManager(getClass().getName()).error("Property \"" + prop + "\" not found!");
            return null;
        }
        return props.getProperty(prop);
    }
}

