package com.epam.unit_tests;

import com.epam.unit_tests.exception.IllegalExpressionException;

public class Calculator {
    String expression;

    public Calculator(String expression) throws IllegalExpressionException {
        this.expression = expression;
        if (!isValid()) throw new IllegalExpressionException("Expression \"" + expression + "\" is invalid");
    }

    public double evaluate() {
        //Избавляемся от квадратных корней:
        String[] words = expression.split(" ");
        for (String word : words) {
            if (!word.matches("^sqrt[0-9]+(\\.[0-9]+)?$")) continue;
            String sqrt = getSqrt(word);
            expression = expression.replaceFirst(word, sqrt);
        }
        //Избавляемся от умножения и деления:
        func(OperationSet.MULTIPLICATION_AND_DIVISION);
        //Складываем и вычитаем:
        func(OperationSet.ADDITION_AND_SUBTRACTION);
        return Double.parseDouble(expression);
    }

    private void func(OperationSet set) {
        outside:
        while (true) {
            for (int i = 0; i < expression.length(); i++) {
                char ch = expression.charAt(i);
                String pattern = "";
                Operation op = null;
                if (set.equals(OperationSet.ADDITION_AND_SUBTRACTION)) {
                    if (!(ch == '+' || ch == '-') || expression.charAt(i + 1) != ' ') continue;
                    pattern = ch == '+' ? Configuration.PATTERN_FOR_ADDITION : Configuration.PATTERN_FOR_SUBTRACTION;
                    op = ch == '+' ? Operation.ADDITION : Operation.SUBTRACTION;
                } else if(set.equals(OperationSet.MULTIPLICATION_AND_DIVISION)) {
                    if (!(ch == '*' || ch == '/')) continue;
                    pattern = ch == '*' ? Configuration.PATTERN_FOR_MULTIPLICATION : Configuration.PATTERN_FOR_DIVISION;
                    op = ch == '*' ? Operation.MULTIPLICATION : Operation.DIVISION;
                }
                double[] nums = getRightAndLeftNumbers(i);
                String item = calculateItem(nums[0], nums[1], op);
                expression = expression.replaceFirst(pattern, item);
                continue outside;
            }
            break;
        }
    }

    private String calculateItem(double first, double second, Operation operation) {
        double result = 0;
        switch (operation) {
            case MULTIPLICATION:
                result = Operations.multiplication(first, second);
                break;
            case DIVISION:
                result = Operations.division(first, second);
                break;
            case ADDITION:
                result = Operations.addition(first, second);
                break;
            case SUBTRACTION:
                result = Operations.subtraction(first, second);
                break;
        }
        return String.valueOf(result);
    }

    private double[] getRightAndLeftNumbers(int relativeChar) {
        int endOfFirst = relativeChar - 1;
        int beginOfSecond = relativeChar + 2;
        int beginOfFirst = endOfFirst - 1;
        while (beginOfFirst != -1 && expression.charAt(beginOfFirst) != ' ') {
            beginOfFirst--;
        }
        beginOfFirst++;
        int endOfSecond = beginOfSecond;
        while (endOfSecond != expression.length() && expression.charAt(endOfSecond) != ' ') {
            endOfSecond++;
        }
        double first = Double.parseDouble(expression.substring(beginOfFirst, endOfFirst));
        double second = Double.parseDouble(expression.substring(beginOfSecond, endOfSecond));
        return new double[]{first, second};
    }

    private String getSqrt(String word) {
        word = word.replaceAll("sqrt", "");
        double d = Double.parseDouble(word);
        return String.valueOf(Operations.squareRoot(d));
    }

    private boolean isValid() {
        String pattern = Configuration.PATTERN_FOR_EXPRESSION;
        return !(expression == null || expression.isEmpty() || !expression.matches(pattern));
    }

    private enum OperationSet {
        ADDITION_AND_SUBTRACTION, MULTIPLICATION_AND_DIVISION
    }
}