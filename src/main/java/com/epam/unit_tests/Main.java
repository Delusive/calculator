package com.epam.unit_tests;

import com.epam.unit_tests.exception.IllegalExpressionException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class Main {
    //По идее, это чудо можно объявить как final, но меня терзают сомнения по этому поводу
    private static DisplayManager out = new DisplayManager(Main.class.getName());

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        out.info(Configuration.WELCOME_MSG);
        while (true) {
            String expr = in.nextLine();
            if (expr.equalsIgnoreCase(Configuration.STRING_TO_EXIT)) break;
            try {
                Calculator calc = new Calculator(expr);
                double result = calc.evaluate();
                NumberFormat nf = new DecimalFormat(Configuration.DECIMAL_FORMAT);
                String msg = Configuration.RESULT_MSG
                        .replaceAll("%result%", nf.format(result))
                        .replaceAll("%exit%", Configuration.STRING_TO_EXIT);
                out.info(msg);
            } catch (IllegalExpressionException ex) {
                out.info(Configuration.INVALID_EXPR_MSG);
            }
        }
    }
}
