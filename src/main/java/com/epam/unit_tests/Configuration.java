package com.epam.unit_tests;

public class Configuration {
    private static final String CFG_FILE_NAME = "/calculator.properties";
    private static final String MSG_FILE_NAME = "/messages.properties";
    private static PropertiesManager cfgProps = new PropertiesManager(CFG_FILE_NAME);
    private static PropertiesManager msgProps = new PropertiesManager(MSG_FILE_NAME);

    //Config:
    public static final String PATTERN_FOR_EXPRESSION = cfgProps.getProperty("pattern-for-expression");
    public static final String PATTERN_FOR_ADDITION = cfgProps.getProperty("pattern-for-addition");
    public static final String PATTERN_FOR_SUBTRACTION = cfgProps.getProperty("pattern-for-subtraction");
    public static final String PATTERN_FOR_MULTIPLICATION = cfgProps.getProperty("pattern-for-multiplication");
    public static final String PATTERN_FOR_DIVISION = cfgProps.getProperty("pattern-for-division");
    public static final String DECIMAL_FORMAT = cfgProps.getProperty("decimal-format");
    public static final String STRING_TO_EXIT = cfgProps.getProperty("string-to-exit");

    //Messages:
    public static final String WELCOME_MSG = msgProps.getProperty("welcome");
    public static final String RESULT_MSG = msgProps.getProperty("result");
    public static final String INVALID_EXPR_MSG = msgProps.getProperty("invalid-expression");

}
