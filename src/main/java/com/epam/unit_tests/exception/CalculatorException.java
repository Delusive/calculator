package com.epam.unit_tests.exception;

public class CalculatorException extends Exception {
    public CalculatorException(String message) {
        super(message);
    }
}
