package com.epam.unit_tests.exception;

import com.epam.unit_tests.Calculator;

public class IllegalExpressionException extends CalculatorException {
    public IllegalExpressionException(String message) {
        super(message);
    }
}
