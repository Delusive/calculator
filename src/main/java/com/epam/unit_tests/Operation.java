package com.epam.unit_tests;

public enum Operation {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, SQUARE_ROOT
}
